<!DOCTYPE HTML>
<!--
	Alpha by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Alpha by HTML5 UP</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="landing">
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header" class="alt">
					<h1><a href="<?php bloginfo( 'url' ); ?>"><?php bloginfo( 'name' ); ?></h1>
					<nav id="nav">
						<ul>
							<li><a href="<?php bloginfo( 'url' ); ?>">На главную</a></li>
							<?php wp_nav_menu(array(
									'theme_location' => '',
									'menu'           => 'Main menu',
									'container'      => '',
									'menu_class'     => 'menu header',
									'echo'           => true,
									'fallback_cb'    => 'wp_page_menu',
									'items_wrap'     => '%3$s',
								));
							?>
							<li><a href="#" class="button">Sign Up</a></li>
						</ul>
					</nav>
				</header>

			<!-- Banner -->
				<section id="banner">
					<h2><?php bloginfo( 'name' ); ?></h2>
					<p><?php bloginfo( 'description' ); ?></p>

				</section>

			<!-- Main -->
				<section id="main" class="container">
					<?php if (have_posts()) { while (have_posts()) { the_post(); ?>
						<section class="box special" <?php post_class(); ?> id="post-<?php the_ID(); ?>">
							<header class="major">
								<h2><?php the_title(); ?></h2>
								<?php //the_content(); ?>
							</header>
							<span class="image featured"><img src="<?php bloginfo( 'template_url' ); ?>/images/pic01.jpg" alt="" /></span>
						</section>



					<?php } /* конец while */ ?>
					<div class="navigation">
						<div class="next-posts"><?php next_posts_link(); ?></div>
						<div class="prev-posts"><?php previous_posts_link(); ?></div>
					</div>
					<?php } else echo "<h2>Записей нет.</h2>"; ?>


					<section class="box special">
						<header class="major">
							<h2>Introducing the ultimate mobile app
							<br />
							for doing stuff with your phone</h2>
							<p>Blandit varius ut praesent nascetur eu penatibus nisi risus faucibus nunc ornare<br />
							adipiscing nunc adipiscing. Condimentum turpis massa.</p>
						</header>
						<span class="image featured"><img src="<?php bloginfo( 'template_url' ); ?>/images/pic01.jpg" alt="" /></span>
					</section>



					<div class="row">


						<div class="6u 12u(narrower)">
							<?php

						 ?>

							<section class="box special">
								<span class="image featured"><img src="<?php bloginfo( 'template_url' ); ?>/images/pic02.jpg" alt="" /></span>
								<h3></h3>
								<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
								<ul class="actions">
									<li><a href="#" class="button alt">Learn More</a></li>
								</ul>
							</section>




						</div>




					</div>

				</section>

			<!-- CTA -->
				<section id="cta">

					<h2>Sign up for beta access</h2>
					<p>Blandit varius ut praesent nascetur eu penatibus nisi risus faucibus nunc.</p>

					<form>
						<div class="row uniform 50%">
							<div class="8u 12u(mobilep)">
								<input type="email" name="email" id="email" placeholder="Email Address" />
							</div>
							<div class="4u 12u(mobilep)">
								<input type="submit" value="Sign Up" class="fit" />
							</div>
						</div>
					</form>

				</section>

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
						<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.min.js"></script>
			<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.dropotron.min.js"></script>
			<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.scrollgress.min.js"></script>
			<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/skel.min.js"></script>
			<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/main.js"></script>

	</body>
</html>
